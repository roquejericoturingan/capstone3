import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { useNavigate } from 'react-router-dom';
import UserContext from '../AppContext';
import '../App.css';

const ProductsCatalogCard = ({ name, price, description, type, productId, navigate }) => {
  const [isHovered, setIsHovered] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const { user } = useContext(UserContext);

  const handleHover = () => {
    setIsHovered(!isHovered);
  };

  const handleOrder = () => {
    if (user.id === null) {
      navigate('/login');
    } else if (user.isAdmin) {
      navigate('/adminProducts');
    } else {
      confirmAlert({
        title: 'Confirm Order',
        message: 'Are you sure you want to place this order?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              const products = [{ productId, quantity }];
              fetch('https://e-commerce-api-36l4.onrender.com/users/checkOut', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                   Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({ products }),
              })
                .then((response) => response.json())
                .then((data) => {
                  console.log(data);
                  confirmAlert({
                    title: 'Success',
                    message: `Placed Order Successful`,
                    buttons: [
                      {
                        label: 'OK',
                        onClick: () => console.log('OK')
                      }
                    ]
                  });
                  // Handle success
                })
                .catch((error) => {
                  console.error(error);
                  // Handle error
                });
            },
          },
          {
            label: 'No',
            onClick: () => {
              // Handle cancel
            },
          },
        ],
      });
    }
  };

  const handleAddToCart = () => {
    if (user.id === null) {
      navigate('/login');
    } else if (user.isAdmin) {
      navigate('/adminProducts');
    } else {
      confirmAlert({
        title: 'Confirm Add to Cart',
        message: 'Are you sure you want to add this item to your cart?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              const products = [{ productId, quantity }];
              fetch('https://e-commerce-api-36l4.onrender.com/users/addToCart', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({ products }),
              })
                .then((response) => response.json())
                .then((data) => {
                  console.log(data);
                  confirmAlert({
                    title: 'Success',
                    message: `Add to Cart Successful`,
                    buttons: [
                      {
                        label: 'OK',
                        onClick: () => console.log('OK')
                      }
                    ]
                  });
                  // Handle success
                })
                .catch((error) => {
                  console.error(error);
                  // Handle error
                });
            },
          },
          {
            label: 'No',
            onClick: () => {
              // Handle cancel
            },
          },
        ],
      });
    }
  };

  return (
    <div className="myCard">
      <div
        className={`innerCard ${isHovered ? 'is-flipped' : ''}`}
        onMouseEnter={handleHover}
        onMouseLeave={handleHover}
      >
        <div className="frontSide">
          <p className="title">{type}</p>
          <h1>{name}</h1>
          <p>₱{price}</p>
        </div>
        <div className="backSide">
          <p className="title">Description</p>
          <p>{description}</p>
          <div>
            <input
              type="number"
              value={quantity}
              onChange={(e) => setQuantity(e.target.value)}
              min={1}
            />
            <button className="orderButton" onClick={handleOrder}>
              Order
            </button>
            <button className="checkoutButton" onClick={handleAddToCart}>
              Add to Cart
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const ProductsCatalog = () => {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetch('https://e-commerce-api-36l4.onrender.com/products/activeProducts')
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleOrder = (productId) => {
    if (user.id === null) {
      navigate('/login');
    } else if (user.isAdmin) {
      navigate('/adminProducts');
    } else {
      const products = [{ productId, quantity: 1 }];
      fetch('https://e-commerce-api-36l4.onrender.com/users/checkOut', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({ products }),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          // Handle success
        })
        .catch((error) => {
          console.error(error);
          // Handle error
        });
    }
  };

  const handleAddToCart = (productId) => {
    if (user.id === null) {
      navigate('/login');
    } else if (user.isAdmin) {
      navigate('/adminProducts');
    } else {
      const products = [{ productId, quantity: 1 }];
      fetch('https://e-commerce-api-36l4.onrender.com/users/addToCart', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({ products }),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          // Handle success
        })
        .catch((error) => {
          console.error(error);
          // Handle error
        });
    }
  };

  return (
    <Container>
      <Row>
        {products.map((product) => (
          <Col key={product._id} sm={6} md={4} lg={3}>
            <ProductsCatalogCard
              name={product.name}
              price={product.price}
              description={product.description}
              type={product.type}
              productId={product._id}
              navigate={navigate} // Pass the navigate function as a prop
            />
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default ProductsCatalog;
