import { useContext, useEffect, useState } from "react";
import { Button, Table, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import UserContext from "../AppContext";
import "../App.css";

export default function AdminDashboard() {
  const [orders, setOrders] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = () => {
    fetch("https://e-commerce-api-36l4.onrender.com/users", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setOrders(data.orders);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      {!user ? (
        <Navigate to="/login" />
      ) : (
        <>
          <Container className="container-dashboard">
            <h2 className="text-center orderhistory">Order History</h2>
            <Table striped bordered hover className="product-table mt-4">
              <thead>
                <tr>
                  <th>Orders</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total Amount</th>
                  <th>Status</th>
                  <th>Paid</th>
                </tr>
              </thead>
              <tbody>
                {orders.map((order, index) => (
                  <tr key={index}>
                    <td>{order.product.map((product) => product.name).join(", ")}</td>
                    <td>{order.product.map((product) => product.quantity).join(", ")}</td>
                    <td>{order.product.map((product) => product.price).join(", ")}</td>
                    <td>{order.totalAmount}</td>
                    <td>{order.isPaid ? "Paid" : "Not Paid"}</td>
                    <td>{order.isPaid ? "Yes" : "No"}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Container>
        </>
      )}
    </>
  );
}
