import { useContext, useEffect, useState } from "react";
import { Button, Table, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import UserContext from "../AppContext";
import "../App.css";

export default function AdminDashboard() {
  const [users, setUsers] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = () => {
    fetch("https://e-commerce-api-36l4.onrender.com/users/all", {
      headers: {
        'Authorization': `Bearer ${user.token}`
      }
    })
      .then(response => response.json())
      .then(data => setUsers(data))
      .catch(error => console.log(error));
  };

  const renderUsers = () => {
    return users.map(user => (
      <tr key={user._id}>
        <td className="break-word">{user.firstName}</td>
        <td className="break-word">{user.lastName}</td>
        <td className="break-word">{user.email}</td>
        <td className="break-word">{user.contactNumber}</td>
        <td className="break-word">{user.createdOn ? formatDate(user.createdOn) : "N/A"}</td>
        <td className="break-word">{user.isAdmin ? "Admin" : "Regular"}</td>
        <td>
          {user.isAdmin ? (
            <Button variant="warning" onClick={() => confirmMakeRegular(user._id)}>
              Make Regular
            </Button>
          ) : (
            <Button variant="primary" onClick={() => confirmMakeAdmin(user._id)}>
              Make Admin
            </Button>
          )}
          <Button variant="danger" onClick={() => confirmDelete(user._id)}>
            Delete
          </Button>
        </td>
      </tr>
    ));
  };

  const formatDate = (dateString) => {
    const options = { year: "numeric", month: "long", day: "numeric" };
    const date = new Date(dateString);
    return date.toLocaleDateString(undefined, options);
  };

  const confirmDelete = (userId) => {
    confirmAlert({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this user?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => deleteUser(userId)
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };

  const deleteUser = (userId) => {
    fetch(`https://e-commerce-api-36l4.onrender.com/users/delete/${userId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      }
    })
      .then(response => {
        if (response.ok) {
          // Remove the deleted user from the state
          setUsers(prevUsers => prevUsers.filter(user => user._id !== userId));
        } else {
          throw new Error('Failed to delete user.');
        }
      })
      .catch(error => console.log(error));
  };


  const confirmMakeAdmin = (userId) => {
    confirmAlert({
      title: 'Confirm Make Admin',
      message: 'Are you sure you want to make this user an admin?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => makeAdmin(userId)
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };

  const makeAdmin = (userId) => {
    fetch(`https://e-commerce-api-36l4.onrender.com/users/information/${userId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify({ isAdmin: true })
    })
      .then(response => {
        if (response.ok) {
          const updatedUsers = users.map(user => {
            if (user._id === userId) {
              return { ...user, isAdmin: true };
            }
            return user;
          });
          setUsers(updatedUsers);
        } else {
          throw new Error('Failed to make user admin.');
        }
      })
      .catch(error => console.log(error));
  };

  const confirmMakeRegular = (userId) => {
    confirmAlert({
      title: 'Confirm Make Regular',
      message: 'Are you sure you want to make this admin a regular user?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => makeRegular(userId)
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };

  const makeRegular = (userId) => {
    fetch(`https://e-commerce-api-36l4.onrender.com/users/information/${userId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      },
      body: JSON.stringify({ isAdmin: false })
    })
      .then(response => {
        if (response.ok) {
          const updatedUsers = users.map(user => {
            if (user._id === userId) {
              return { ...user, isAdmin: false };
            }
            return user;
          });
          setUsers(updatedUsers);
        } else {
          throw new Error('Failed to make user regular.');
        }
      })
      .catch(error => console.log(error));
  };

  return (
    <>
      {!user ? (
        <Navigate to="/login" />
      ) : (
        <>
          <Container className="container-dashboard">
            <h2 className="text-center">BVC USERS</h2>
            <Table striped bordered hover className="product-table mt-4">
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Contact Number</th>
                  <th>Created On</th>
                  <th>Admin Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{renderUsers()}</tbody>
            </Table>
          </Container>
        </>
      )}
    </>
  );
}
