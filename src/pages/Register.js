import React, { useState, useEffect } from 'react';
import { MDBBtn, MDBContainer, MDBCard, MDBCardBody, MDBCardImage, MDBRow, MDBCol, MDBIcon, MDBInput } from 'mdb-react-ui-kit';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Alert from '@mui/material/Alert';
import '../App.css';

function Register() {
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [contactNumber, setContactNumber] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [user, setUser] = useState({ id: null }); 

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      contactNumber.length === 11 &&
      password !== '' &&
      password2 !== '' &&
      password === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, contactNumber, password, password2]);

  useEffect(() => {
    // Check if the user is already logged in
    const loggedInUser = localStorage.getItem('user');
    if (loggedInUser) {
      const user = JSON.parse(loggedInUser);
      setUser(user);
    }
  }, []);

 function registerUser(e) {
   e.preventDefault();

   fetch(`https://e-commerce-api-36l4.onrender.com/users/register`, {
     method: 'POST',
     headers: {
       'Content-Type': 'application/json',
     },
     body: JSON.stringify({
       email: email,
       firstName: firstName,
       lastName: lastName,
       contactNumber: contactNumber,
       password: password,
     }),
   })
     .then((res) => {
       if (res.status === 200) {
         return res.json();
       } else {
         console.log('Registration error response:', res);
         return Promise.reject(new Error('Something went wrong'));
       }
     })
     .then((data) => {
       console.log('Registration response:', data);
       if (data.message === 'Email already exists') {
         // Display duplicate email alert
         confirmAlert({
           title: 'Error',
           message: 'Duplicate Email. Provide another email.',
           buttons: [
             {
               label: 'OK',
             },
           ],
         });
       } else if (data.message.startsWith('Hello')) {
         setFirstName('');
         setLastName('');
         setEmail('');
         setContactNumber('');
         setPassword('');
         setPassword2('');
         // Display success alert
         confirmAlert({
           title: 'Success',
           message: 'Registration Successful!',
           buttons: [
             {
               label: 'OK',
               onClick: () => navigate('/login'),
             },
           ],
         });
       } else {
         return Promise.reject(new Error('Something went wrong'));
       }
     })
     .catch((error) => {
       console.error(error);
       // Display generic error alert
       confirmAlert({
         title: 'Error',
         message: 'Something went wrong, please try again',
         buttons: [
           {
             label: 'OK',
           },
         ],
       });
     });
 }


  return (
    user.id !== null ? (
      user.isAdmin ? (
        <Navigate to="/admin" />
      ) : (
        <Navigate to="/products" />
      )
    ) : (
      <MDBContainer className="my-5">
        <MDBCard>
          <MDBRow className="g-0">
            <MDBCol md="6">
              <MDBCardImage
                src="https://img.freepik.com/premium-photo/white-coffee-cup-lies-roasted-coffee-beans-highest-standard_152625-9286.jpg?w=2000"
                alt="login form"
                className="rounded-start w-100 d-none d-sm-block"
              />
            </MDBCol>
            <MDBCol md="6">
              <MDBCardBody className="d-flex flex-column">
                <div className="d-flex flex-row register">
                  <MDBIcon fas icon="cubes fa-3x me-3" style={{ color: '#ff6219' }} />
                </div>
                <h5 className="fw-normal my-4 pb-3 text-center" style={{ letterSpacing: '1px' }}>
                  Register Now
                </h5>
                <form onSubmit={registerUser}>
                  <MDBInput
                    wrapperClass="mb-4"
                    label="First Name"
                    id="formControlLg"
                    type="text"
                    size="lg"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Last Name"
                    id="formControlLg"
                    type="text"
                    size="lg"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Contact Number"
                    id="formControlLg"
                    type="text"
                    size="lg"
                    value={contactNumber}
                    onChange={(e) => setContactNumber(e.target.value)}
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Email address"
                    id="formControlLg"
                    type="email"
                    size="lg"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Password"
                    id="formControlLg"
                    type="password"
                    size="lg"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Verify Password"
                    id="formControlLg"
                    type="password"
                    size="lg"
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                  />
                  {isActive ? (
                    <Button variant="primary" type="submit">
                      Submit
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" disabled>
                      Submit
                    </Button>
                  )}
                </form>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        </MDBCard>
      </MDBContainer>
    )
  );
}

export default Register;
