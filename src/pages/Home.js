import React from 'react';
import { Helmet } from 'react-helmet';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './Home.css';
import carousel1 from '../carousel-1.jpg';
import carousel2 from '../carousel-2.jpg';
import carousel3 from '../carousel-3.jpg';
import carousel4 from '../carousel-4.jpg';
import carousel5 from '../carousel-5.jpg';
import carousel6 from '../carousel-6.jpg';
import carousel7 from '../carousel-7.jpg';
import carousel8 from '../carousel-8.jpg';
import carousel9 from '../carousel-9.jpg';
import bgHome from '../bg-home.jpg';

const Home = () => {
  return (
    <div>

      <Helmet>
             <link rel="preconnect" href="https://fonts.googleapis.com" />
             <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
             <link
               href="https://fonts.googleapis.com/css2?family=Black+Ops+One&family=Dancing+Script&family=Great+Vibes&family=Pacifico&display=swap"
               rel="stylesheet"
             />

            <link rel="preconnect" href="https://fonts.googleapis.com"/>
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
            <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&family=Pacifico&family=Playfair+Display:wght@500&display=swap" rel="stylesheet"/>stylesheet"/>
      </Helmet>

      <div className="carousel-container">
      <div className="welcomePage">
      <h1 className="text-center welcomePage1">Welcome to Brew Voyage Cafe</h1>
      <p className="text-center welcomePage2">Embark on a flavorful journey!</p>
      </div>

      <Carousel autoPlay infiniteLoop showArrows={false} showStatus={false} showThumbs={false} showIndicators={false}>

        <div>
          <img className="carousel-image" src={carousel1} alt="Image 1" />
        </div>

        <div>
          <img className="carousel-image" src={carousel2} alt="Image 2" />
        </div>

        <div>
          <img className="carousel-image" src={carousel3} alt="Image 3" />
        </div>

        <div>
          <img className="carousel-image" src={carousel4} alt="Image 4" />
        </div>

        <div>
          <img className="carousel-image" src={carousel5} alt="Image 5" />
        </div>

        <div>
          <img className="carousel-image" src={carousel6} alt="Image 6" />
        </div>

        <div>
          <img className="carousel-image" src={carousel7} alt="Image 7" />
        </div>

        <div>
          <img className="carousel-image" src={carousel8} alt="Image 8" />
        </div>

        <div>
          <img className="carousel-image" src={carousel9} alt="Image 9" />
        </div>

      </Carousel>
      </div>
    </div>

  );
};

export default Home;
