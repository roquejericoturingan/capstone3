import { useContext, useEffect, useState } from "react";
import { Button, Table, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import UserContext from "../AppContext";
import "../App.css";

export default function AddToCart() {
  const [addToCart, setAddToCart] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetchAddToCart();
  }, []);

  const fetchAddToCart = () => {
    fetch("https://e-commerce-api-36l4.onrender.com/users", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setAddToCart(data.addToCart);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      {!user ? (
        <Navigate to="/login" />
      ) : (
        <>
          <Container className="container-dashboard">
            <h2 className="text-center addToCart">Add to Cart</h2>
            <Table striped bordered hover className="product-table mt-4">
              <thead>
                <tr>
                  <th>Products</th>
                  <th>Quantity</th>
                  <th>Total Amount</th>
                </tr>
              </thead>
              <tbody>
                {addToCart.map((cart, index) => (
                  <tr key={index}>
                    <td>{cart.product.map((product) => product.name).join(", ")}</td>
                    <td>{cart.product.map((product) => product.quantity).join(", ")}</td>
                    <td>{cart.totalAmount}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Container>
        </>
      )}
    </>
  );
}
